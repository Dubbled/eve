package irc

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

type Connection struct {
	nick         string
	server       string
	pass         string
	procWrite    chan string
	socket       net.Conn
	Timeout      time.Duration
	errorChan    chan error
	LastReceived time.Time
	Log          *log.Logger
}

func (irc *Connection) Read() {
	reader := bufio.NewReader(irc.socket)
	for {
		msg, err := reader.ReadString('\n')
		irc.socket.SetReadDeadline(time.Now().Add(irc.Timeout))
		if err == nil {
			event, err := irc.NewEvent(msg)
			if err != nil {
				errors.New(fmt.Sprintf("Failed to parse event: %s", msg))
			}
			// fmt.Print(msg) // Debug
			if err = event.Run(); err != nil {
				irc.errorChan <- err
			}
		} else {
			irc.errorChan <- errors.New(fmt.Sprintf("Read Timeout on %s", irc.server)) // Send error to be handled
		}
	}
}

func (irc *Connection) UpdateTime() {
	irc.LastReceived = time.Now()
}

func (irc *Connection) Write() {
	for out := range irc.procWrite {
		irc.socket.SetWriteDeadline(time.Now().Add(irc.Timeout))
		_, err := irc.socket.Write([]byte(out))
		if err != nil {
			irc.errorChan <- err
		}
	}
}

func (irc *Connection) Send(msg string) {
	irc.procWrite <- msg + "\r\n"
}

func (irc *Connection) PingLoop() {
	msg := fmt.Sprintf("PING %s", irc.server)
	for {
		irc.Send(msg)
		time.Sleep(time.Minute * 3)
	}
}

func (irc *Connection) ErrorHandle() { // Log any errors gathered from channel
	for err := range irc.errorChan {
		irc.Log.Printf("Error: %s\n", err)
	}
}

func (irc *Connection) Terminate() {
	irc.socket.Close()
}

func (irc *Connection) Connect() error {
	logFile, err := os.Create(fmt.Sprintf("%s@%s.log", irc.nick, irc.server))
	irc.Log = log.New(logFile, "", 0)

	// Unencrypted
	socket, err := net.DialTimeout("tcp", "irc.tripsit.me:6667", time.Minute*1)
	irc.socket = socket
	if err != nil {
		return err
	}
	fmt.Printf("Connected to %s\n", irc.server)
	irc.Log.Printf("Connected to %s\n", irc.server)
	irc.procWrite = make(chan string, 5)
	irc.errorChan = make(chan error, 2)
	go irc.Read()
	go irc.Write()
	go irc.PingLoop()
	go irc.ErrorHandle()
	return nil
}

func (irc *Connection) Register() {
	irc.procWrite <- fmt.Sprintf("NICK %s\r\n", irc.nick)
	irc.procWrite <- fmt.Sprintf("USER %s 0 * :%s\r\n", irc.nick, irc.nick)
}

func (irc *Connection) Join(channel string) {
	irc.procWrite <- fmt.Sprintf("JOIN %s\r\n", channel)
}

func (irc *Connection) Part(channel string) {
	irc.procWrite <- fmt.Sprintf("PART %s\r\n", channel)
}

func (irc *Connection) Nick(nick string) {
	irc.procWrite <- fmt.Sprintf("NICK %s\r\n", nick)
}

func IRC(nick, server, password string) *Connection {
	irc := &Connection{
		nick:    nick,
		server:  server,
		Timeout: 1 * time.Minute,
		pass:    password,
	}
	return irc
}
