package irc

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

type Event struct {
	Code       string
	Host       string
	Raw        string
	Source     string
	Nick       string
	User       string
	Args       []string
	Connection *Connection
}

// Reimplement
func (irc *Connection) NewEvent(msg string) (*Event, error) {
	msg = strings.TrimSuffix(msg, "\r\n")
	event := &Event{Connection: irc}
	if len(msg) < 5 {
		return nil, errors.New("Malformed msg from server")
	}
	if msg[0] == ':' {
		if i := strings.Index(msg, " "); i > -1 {
			event.Source = msg[1:i]
			msg = msg[i+1:]

		} else {
			return nil, errors.New("Malformed msg from server")
		}

		if i, j := strings.Index(event.Source, "!"), strings.Index(event.Source, "@"); i > -1 && j > -1 && i < j {
			event.Nick = event.Source[0:i]
			event.User = event.Source[i+1 : j]
			event.Host = event.Source[j+1 : len(event.Source)]
		}
	}

	split := strings.SplitN(msg, " :", 2)
	args := strings.Split(split[0], " ")
	event.Code = strings.ToUpper(args[0])
	event.Args = args[1:]
	if len(split) > 1 {
		event.Args = append(event.Args, split[1])
	}
	event.Connection.LastReceived = time.Now()
	return event, nil
}

func (event *Event) Run() error {
	/* TODO:
	Receive channel permissions and save somewhere? Code 353
	*/
	if event.Code == "451" {
		event.Connection.Register()
	} else if event.Code == "376" { // MOTD Finished
		event.Connection.Join("#glob")
	} else if event.Code == "PING" {
		resp := fmt.Sprintf("PONG %s", event.Host)
		event.Connection.Send(resp)
	} else if event.Code == "PONG" {
		event.Connection.UpdateTime()
	} else if event.Code == "INVITE" {
		event.Connection.Join(event.Args[1])
	} else if event.Code == "PRIVMSG" { // Repeat any messages in channel.
		msg := fmt.Sprintf(":%s PRIVMSG %s :%s", event.Connection.nick, event.Args[0], event.Args[1])
		event.Connection.Send(msg)
	} else if event.Code == "NOTICE" {
		event.Connection.Log.Printf("notice: %s", event.Args[1])
	} else if event.Code == "372" || event.Code == "375" || event.Code == "JOIN" { // Ignore
		event.Connection.Log.Printf(fmt.Sprintf("Ignoring event: %s", event.Code))
	} else {
		return errors.New(fmt.Sprintf("Unhandled event: %s", event.Code))
	}
	return nil
}
